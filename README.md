# Frontend Mentor - Interactive rating card component solution

This is a solution to the [Age calculator app on Frontend Mentor](https://www.frontendmentor.io/challenges/age-calculator-app-dF9DFFpj-Q). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

### The challenge

Your users should be able to: 

- View an age in years, months, and days after submitting a valid date through the form
- Receive validation errors if:
  - Any field is empty when the form is submitted
  - The day number is not between 1-31
  - The month number is not between 1-12
  - The year is in the future
  - The date is invalid e.g. 31/04/1991 (there are 30 days in April)
- View the optimal layout for the interface depending on their device's screen size
- See hover and focus states for all interactive elements on the page
- **Bonus**: See the age numbers animate to their final number when the form is submitted

### Links

- Live Site URL: [Live Solution](https://frontendchallenges.gitlab.io/age-calculator/) 
- Screenshot: [Entertainment](./screenshot.png)

## My process

1. Create project `npx nuxi init <project-name>`
   #Add dependencies `yarn add --dev sass`
   Edit nuxt.config.ts to set the base publish URL
   ``app.baseURL: "/age-calculator/" ,```   
   Also to add tokens.scss to the global css
2. Design the html with no style by now
  index.vue
    DateInput
      article
        div*3>lb+input
        div.division
          button
            img
    AgeDisplayer
      article
        p*3>span.number+span.label
3. Add the backend logic to make the site work (the UI is ugly but the site starts to work). JS to calculate the years, months and days
2. Create tokens.scss, colors.css, components/(links,nav,buttons,...) index to import the previous files
7. Notice and implement the active states.
8. Set layout/spacing for tablet and desktop devices
9. Deploy to gitlab pages

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow
- Nuxt3 + Vue + Nitro
- Pinia
- Netlify

### Lessons

How to add a linter:
1. yarn add eslint --dev
2. Add a script in the package.json file "lint": "eslint",
3. yarn lint --init
4. Answer the questions.
5. Set the script in  package.json file "lint": "eslint ./src --fix",
6. Finally to format on Save in vscode, add the following to .vscode/settings.json
```
    "editor.codeActionsOnSave": {
        "source.fixAll.eslint": true
    },
```

How to create a nuxt project

1. npx nuxi init <project-name>
2. code <project-name>
3. yarn install
4. yarn dev -o (to execute the project in dev mode)
5. Set the default layout (layouts/default.vue), add a pages/index.vue and set app.vue to call NuxtLayout and NuxtPage

How to add global styles in nuxt
1. Edit nuxt.config.ts to add 
```
vite: {    css: {      preprocessorOptions: {        scss: {          additionalData: '@use "@/assets/css/tokens.scss" as *;'        }      }    }  }
```

Locally preview production build:

```bash
npm run preview | yarn preview
```
*How to set pinia in nuxt*

1. yarn add pinia @pinia/nuxt
2. Create a stores/index.ts file
3. In nuxt.config.ts, add 
```
modules: [
    // ...
    '@pinia/nuxt',
  ],
```

Deploy to netlify
https://dev.to/sidhantpanda/self-hosted-gitlab-continuous-deployment-to-netlify-49lk, 
https://medium.com/js-dojo/deploying-vue-js-to-netlify-using-gitlab-continuous-integration-pipeline-1529a2bbf170

### Continued development

Implementing this project in Nuxt


### Useful resources
Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) 
- [How to deploy a Static Site](https://vitejs.dev/guide/static-deploy.html)
- [Created a modal](https://www.digitalocean.com/community/tutorials/vuejs-vue-modal-component)
npx run lerna build

## Author

- Website - [Víctor HG](https://gitlab.com/vianhg)
- Frontend Mentor - [@vianhg](https://www.frontendmentor.io/profile/vianhg)

## Acknowledgments

Always to God and my family.
