// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  css: ['@/assets/css/index.scss'],
  app: {
    baseURL: "/age-calculator/",
    head: {
      title: 'Age Calcultator App',
      htmlAttrs: {
        lang: 'en'
      },
      meta: [
        { name: 'description', content: 'Age Calcultator Tool.' }
      ],
      link: [
        {
          rel: 'icon',
          type: 'image/png',
          sizes: '32x32',
          href: '/assets/images/favicon-32x32.ico',
        },
        { rel: 'preconnect', href: 'https://fonts.googleapis.com' },
        { rel: 'preconnect', href: 'https://fonts.gstatic.com' },
        {
          rel: 'stylesheet',
          href: 'https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,700;1,400;1,800&display=swap',
        },
      ],
    }
  },
  vite: { css: { preprocessorOptions: { scss: { additionalData: '@use "@/assets/css/_tokens.scss" as *;' } } } },
})
